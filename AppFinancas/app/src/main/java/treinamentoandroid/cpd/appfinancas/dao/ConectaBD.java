package treinamentoandroid.cpd.appfinancas.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 *
 * DATA DEFINITION LANGUAGE -> MEXE NA ESTRUTURA DO BANCO
 * Created by gabrieldoandroid on 02/06/17.
 */

public class ConectaBD extends SQLiteOpenHelper {

    private static ConectaBD minhaInstancia;
    public static final String NOME_DO_BANCO_DDADOS = "dbobs";
    public static final int VERSAO_OFICIAL = 42;
    private String sqlPraCriarTabela;

    private ConectaBD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public static ConectaBD recuperaMinhaInstancia(Context context) {
        if(minhaInstancia == null){
            minhaInstancia = new ConectaBD(context, NOME_DO_BANCO_DDADOS, null, VERSAO_OFICIAL);
        }
        return minhaInstancia;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        sqlPraCriarTabela = "CREATE TABLE " + DespesaDAO.NOME_DA_TABELA +
                "("+ DespesaDAO.VK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DespesaDAO.VK_DESCRICAO + " TEXT NOT NULL, "+
                DespesaDAO.VK_DATA + " TEXT NOT NULL, " +
                DespesaDAO.VK_VALOR + " REAL NOT NULL, " +
                DespesaDAO.VK_ENDERECO + " TEXT NOT NULL);";
        db.execSQL(sqlPraCriarTabela);

        Log.i("ConectaBD", "Banco tá bombando");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
