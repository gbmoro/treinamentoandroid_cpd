package treinamentoandroid.cpd.appfinancas.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

import treinamentoandroid.cpd.appfinancas.model.Despesa;

/**
 * DML - DATA MANIPULATION LANGUAGE - Insert, update...
 *
 * Created by gabrieldoandroid on 02/06/17.
 */

public class DespesaDAO {

    ConectaBD conectaBD;
    public static final String NOME_DA_TABELA="Despesa";
    public static String VK_DESCRICAO = "descricao";
    public static String VK_VALOR = "valor";
    public static String VK_ID = "id";
    public static String VK_DATA = "data";
    public static String VK_ENDERECO = "endereco";

    public DespesaDAO(Context context) {
        conectaBD = ConectaBD.recuperaMinhaInstancia(context);
    }

    public boolean delete(Despesa despesa) {

        SQLiteDatabase objSqlite = conectaBD.getWritableDatabase();

        try {
            objSqlite.delete(NOME_DA_TABELA, VK_ID + "=?",
                    new String[]{String.valueOf(despesa.id)}
            );

        } catch (Exception erro) {
            return false;
        }

        objSqlite.close();

        return true;

    }

    private ContentValues converteObjetoParaContent(Despesa despesa) {

        ContentValues pacoteDeValores = new ContentValues();
        pacoteDeValores.put(VK_DESCRICAO, despesa.descricao);
        pacoteDeValores.put(VK_VALOR, despesa.valor);
        pacoteDeValores.put(VK_DATA, despesa.data);
        pacoteDeValores.put(VK_ENDERECO, despesa.endereco);

        if(despesa.id>0) {
            pacoteDeValores.put(VK_ID, despesa.id);
        }
        return pacoteDeValores;
    }

    public long update(long id, Despesa despesa) {
        long chavePrimaria = 0;

        SQLiteDatabase objSqlite = conectaBD.getWritableDatabase();

        try {

            ContentValues pacoteDeValores = converteObjetoParaContent(despesa);
            chavePrimaria = objSqlite.update(NOME_DA_TABELA,pacoteDeValores, VK_ID + "=?",
                    new String[]{String.valueOf(id)});

            if (chavePrimaria > 0) {
                Log.i("DespesaDAO", "Bombou!");
            }

        }catch(Exception erro) {
            Log.e("DespesaDAO", erro.getMessage());

        } finally {
            objSqlite.close();
        }

        return chavePrimaria;
    }

    public long insert(Despesa despesa) {
        long chavePrimaria = 0;

        SQLiteDatabase objSqlite = conectaBD.getWritableDatabase();

        try {

            ContentValues pacoteDeValores = converteObjetoParaContent(despesa);
            chavePrimaria = objSqlite.insert(NOME_DA_TABELA, null, pacoteDeValores);

            if (chavePrimaria > 0) {
                Log.i("DespesaDAO", "Bombou!");
            }

        }catch(Exception erro) {
            Log.e("DespesaDAO", erro.getMessage());

        } finally {
            objSqlite.close();
        }

        return chavePrimaria;
    }

    public List<Despesa> select() {
        List<Despesa> listaDeDespesas = new ArrayList<Despesa>();

        String query = "SELECT * FROM " + NOME_DA_TABELA + ";";

        SQLiteDatabase db = conectaBD.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        Despesa despesaTmp = null;
        Double valor = null;
        long id = -1;
        String descricao = "", dataT = "", endereco = "";

        while (c.moveToNext()) {
            id = c.getLong(c.getColumnIndex(VK_ID));
            valor = c.getDouble(c.getColumnIndex(VK_VALOR));
            descricao = c.getString(c.getColumnIndex(VK_DESCRICAO));
            dataT = c.getString(c.getColumnIndex(VK_DATA));
            endereco = c.getString(c.getColumnIndex(VK_ENDERECO));
            despesaTmp = new Despesa(id, descricao, valor, dataT, endereco);

            listaDeDespesas.add(despesaTmp);

        }

        c.close();
        db.close();

        Log.i(getClass().getName(), "Quantas despesas foram cadastradas - "+ listaDeDespesas.size());

        return listaDeDespesas;
    }

}
