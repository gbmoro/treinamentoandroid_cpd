package treinamentoandroid.cpd.appfinancas.model;

import java.io.Serializable;

/**
 * Created by gabrieldoandroid on 02/06/17.
 */

public class Despesa implements Serializable{

    public long id;
    public String descricao;
    public Double valor;
    public String data;
    public String endereco;

    public Despesa(long idT, String desc, Double valorT, String dataT, String enderecoT) {
        this.id= idT;
        this.descricao = desc;
        this.valor = valorT;
        this.data = dataT;
        this.endereco = enderecoT;
    }

    @Override
    public String toString() {
        return descricao + ", " + valor;
    }
}
