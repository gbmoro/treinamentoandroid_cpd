package treinamentoandroid.cpd.appfinancas.model;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.net.ContentHandler;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by gabrieldoandroid on 21/07/17.
 */

public class Util {

    public static String retornaDataAtual() {

        /*Retorna data atual do Java*/
        Calendar c = Calendar.getInstance();
        Date dataObj = c.getTime();

        /*Formatar a data em determinado padrao*/
        SimpleDateFormat formatadorDeData = new SimpleDateFormat("dd/MM/yyyy");
        return formatadorDeData.format(dataObj);
    }

}
