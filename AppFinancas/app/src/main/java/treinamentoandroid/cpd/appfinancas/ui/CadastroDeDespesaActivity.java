package treinamentoandroid.cpd.appfinancas.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import java.io.Serializable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import treinamentoandroid.cpd.appfinancas.model.Despesa;
import treinamentoandroid.cpd.appfinancas.dao.DespesaDAO;
import treinamentoandroid.cpd.appfinancas.R;
import treinamentoandroid.cpd.appfinancas.model.Util;

public class CadastroDeDespesaActivity extends AppCompatActivity {

    @BindView(R.id.txtDescricao)
    EditText txtDescricao;
    @BindView(R.id.txtValor)
    EditText txtValor;
    @BindView(R.id.data)
    EditText txtData;
    @BindView(R.id.endereco)
    EditText txtEndereco;
    @BindView(R.id.btnSalvar)
    Button btnSalvar;

    Despesa despesaRecebida;
    DespesaDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_de_despesa);

        ButterKnife.bind(this);

        dao = new DespesaDAO(this);

        Log.i("EditText txtDescricao", String.valueOf(txtDescricao.hashCode()));
        Log.i("EditText txtValor", String.valueOf(txtValor.hashCode()));
        Log.i("Button btnCadastrar", String.valueOf(btnSalvar.hashCode()));

        txtData.setText(Util.retornaDataAtual());

        Serializable pacote = getIntent().getSerializableExtra(MainActivity.VK_EDITAR);
        if(pacote!=null){
            despesaRecebida = (Despesa) pacote;
            txtDescricao.setText(despesaRecebida.descricao);
            txtValor.setText(String.valueOf(despesaRecebida.valor));
            txtEndereco.setText(despesaRecebida.endereco);
        }

    }

    @OnClick(R.id.btnSalvar)
    public void eventoDoBtnCadastrar() {

        Despesa novaDespesa =
                new Despesa(-1, txtDescricao.getText().toString(),
                        Double.parseDouble(txtValor.getText().toString()),
                        txtData.getText().toString(),
                        txtEndereco.getText().toString());

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alerta de Usuario");

        if(despesaRecebida == null) {

            dao.insert(novaDespesa);

            builder.setMessage("Voce registrou R$ " + novaDespesa.valor);
            builder.show();

        }else {
            despesaRecebida.descricao = txtDescricao.getText().toString();
            despesaRecebida.valor = Double.parseDouble(txtValor.getText().toString());

            long retorno = dao.update(despesaRecebida.id, novaDespesa);
            if(retorno>0){
                builder.setMessage("Voce atualizou o registro de descricao: " +
                        despesaRecebida.descricao);
                builder.show();
            }
        }


        startActivity(new Intent(this, MainActivity.class));

    }


}
