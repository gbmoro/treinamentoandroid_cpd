package treinamentoandroid.cpd.appfinancas.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import treinamentoandroid.cpd.appfinancas.R;
import treinamentoandroid.cpd.appfinancas.model.Despesa;

/**
 * Created by gabrieldoandroid on 23/06/17.
 */

public class ListaDeDespesasAdapter extends BaseAdapter{

    List<Despesa> despesaList;
    MainActivity activity;
    Context mContext;
    Picasso picasso;

    public ListaDeDespesasAdapter(MainActivity tela, List<Despesa> despesas) {
        this.activity = tela;
        this.mContext = this.activity.getApplicationContext();
        this.despesaList = despesas;
        picasso = new Picasso.Builder(activity.getApplication()).build();
    }

    @Override
    public int getCount() {
        return despesaList.size();
    }

    @Override
    public Object getItem(int position) {
        return despesaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Despesa despesaTmp =  (Despesa) this.getItem(position);

        View view;

        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            view = inflater.inflate(R.layout.layout_despesa_item, parent, false);
        } else {
            view = convertView;
        }

        TextView txtDescricaoItem = (TextView) view.findViewById(R.id.txtDescricao_item);
        TextView txtValorItem = (TextView) view.findViewById(R.id.txtValor_item);
        TextView txtDataItem = (TextView) view.findViewById(R.id.txtData_item);

        ImageView imgView = (ImageView) view.findViewById(R.id.imageDoItem);

        txtDescricaoItem.setText(despesaTmp.descricao);
        txtValorItem.setText(String.valueOf(despesaTmp.valor));
        txtDataItem.setText(String.valueOf(despesaTmp.data));

        picasso.with(mContext)
                .load("https://api.adorable.io/avatars/200/"+txtDescricaoItem+".png")
                .into(imgView);

        return view;
    }
}
