package treinamentoandroid.cpd.appfinancas.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import treinamentoandroid.cpd.appfinancas.dao.DespesaDAO;
import treinamentoandroid.cpd.appfinancas.R;
import treinamentoandroid.cpd.appfinancas.model.Despesa;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.listaDeDespesas)
    ListView listaDeDespesas;
    @BindView(R.id.btnDaGoogleA)
    Button botaoDeCadastro;
    DespesaDAO despesaDAO;
    ListaDeDespesasAdapter adapter;
    public static final String VK_EDITAR = "editar";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        despesaDAO = new DespesaDAO(this);
        registerForContextMenu(listaDeDespesas);
        atualizaList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        atualizaList();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        final Despesa despesa = (Despesa) listaDeDespesas.getItemAtPosition(info.position);


        MenuItem itemDeletar = menu.add("Deletar");
        itemDeletar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(despesaDAO.delete(despesa)){
                    atualizaList();
                    Toast.makeText(MainActivity.this,
                            "A despesa " + despesa.descricao +
                            " foi deletada com sucesso!", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(MainActivity.this,
                            "Náo rolou...",
                            Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        MenuItem itemEditar = menu.add("Editar");
        itemEditar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent vaiParaCadastro = new Intent(MainActivity.this,
                        CadastroDeDespesaActivity.class);
                vaiParaCadastro.putExtra(VK_EDITAR, despesa);
                startActivity(vaiParaCadastro);
                return false;
            }
        });
        
        MenuItem itemIrParaMap = menu.add("Ir para o Mapa");
        itemIrParaMap.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent vaiParaMapa = new Intent(MainActivity.this,
                        MapsActivity.class);
                vaiParaMapa.putExtra(MapsActivity.CHAVE, despesa);
                startActivity(vaiParaMapa);
                return false;
            }
        });

    }

    private void atualizaList() {

        List<Despesa> listaDeDespesasDoBanco = despesaDAO.select();

        this.adapter = new ListaDeDespesasAdapter(this, listaDeDespesasDoBanco);
        this.listaDeDespesas.setAdapter(this.adapter);
    }


    @OnClick(R.id.btnDaGoogleA)
    public void criarCadastro() {
        Intent vaiParaCadastro = new Intent(this, CadastroDeDespesaActivity.class);
        startActivity(vaiParaCadastro);
    }

}
