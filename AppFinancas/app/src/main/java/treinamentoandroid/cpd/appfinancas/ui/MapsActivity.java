package treinamentoandroid.cpd.appfinancas.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.List;
import java.util.Locale;
import treinamentoandroid.cpd.appfinancas.R;
import treinamentoandroid.cpd.appfinancas.model.Despesa;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public static final String CHAVE = "despesa";
    private Despesa despesa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        despesa = (Despesa) getIntent().getExtras().get(CHAVE);
        Log.i("ehTois", despesa.toString());
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } // Add a marker in Sydney and move the camera
        LatLng latLngDaDespesa = retornaLatitudeLongitude(despesa.endereco);
        mMap.addMarker(new MarkerOptions().position(latLngDaDespesa).title(despesa.descricao + " - " + despesa.valor));
        mMap.setMyLocationEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngDaDespesa));
    }

private LatLng retornaLatitudeLongitude(String endereco) {
    LatLng posicao = null;

    try {
        Geocoder geocoder = new Geocoder(this);

        List<Address> resultados = geocoder.getFromLocationName(endereco, 5);
        Address address = resultados.get(0);
        if (address != null) {
            posicao = new LatLng(address.getLatitude(),
                    address.getLongitude());
        } else {
            posicao = new LatLng(-30.041021, -51.206098);
        }
    } catch (Exception e) {
        Log.e("Classe Util", e.getMessage());
    }
        return posicao;
    }

}
