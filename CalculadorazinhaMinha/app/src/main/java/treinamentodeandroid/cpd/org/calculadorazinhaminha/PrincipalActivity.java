package treinamentodeandroid.cpd.org.calculadorazinhaminha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PrincipalActivity extends AppCompatActivity {

    TextView txtVisor;
    Button btn1, btn2, btn3, btn4,
            btn5, btn6, btn7, btn8,
            btn9, btn0, btnCE, btnIgual,
            btnMult, btnDiv, btnSub,
            btnSoma, btnPoint;
    Calculadora calculadora;
    double num1, num2;
    TipoDeConta tipoDeConta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        calculadora = new Calculadora();

        recuperaElementos();
        registraEventos();

        limpaVisor();

    }

    private void registraEventos() {
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "1";
                txtVisor.setText(textoDoVisor);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "2";
                txtVisor.setText(textoDoVisor);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "3";
                txtVisor.setText(textoDoVisor);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "4";
                txtVisor.setText(textoDoVisor);
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "5";
                txtVisor.setText(textoDoVisor);
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "6";
                txtVisor.setText(textoDoVisor);
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "7";
                txtVisor.setText(textoDoVisor);
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "8";
                txtVisor.setText(textoDoVisor);
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "9";
                txtVisor.setText(textoDoVisor);
            }
        });
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + "0";
                txtVisor.setText(textoDoVisor);
            }
        });
        btnCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpaVisor();
            }
        });
        btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipoDeConta = TipoDeConta.SOMA;
                num1 = Double.parseDouble(txtVisor.getText().toString());
                limpaVisor();
            }
        });
        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipoDeConta = TipoDeConta.SUBTRACAO;
                num1 = Double.parseDouble(txtVisor.getText().toString());
                limpaVisor();
            }
        });
        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipoDeConta = TipoDeConta.MULTIPLICACAO;
                num1 = Double.parseDouble(txtVisor.getText().toString());
                limpaVisor();
            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipoDeConta = TipoDeConta.DIVISAO;
                num1 = Double.parseDouble(txtVisor.getText().toString());
                limpaVisor();
            }
        });
        btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double result = 0.0;
                num2 = Double.parseDouble(txtVisor.getText().toString());
                result = calculadora.fazerCalculo(num1, num2,tipoDeConta);
                txtVisor.setText(String.valueOf(result));
            }
        });
        btnPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoDoVisor = txtVisor.getText().toString();
                textoDoVisor = textoDoVisor + ".";
                txtVisor.setText(textoDoVisor);
            }
        });
    }

    private void recuperaElementos() {
        /*Recuperando o label do visor*/
        txtVisor = (TextView) findViewById(R.id.txtEquacao);
        /*Recuperando os botões númericos*/
        btn1 = (Button) findViewById(R.id.btn_num1);
        btn2 = (Button) findViewById(R.id.btn_num2);
        btn3 = (Button) findViewById(R.id.btn_num3);
        btn4 = (Button) findViewById(R.id.btn_num4);
        btn5 = (Button) findViewById(R.id.btn_num5);
        btn6 = (Button) findViewById(R.id.btn_num6);
        btn7 = (Button) findViewById(R.id.btn_num7);
        btn8 = (Button) findViewById(R.id.btn_num8);
        btn9 = (Button) findViewById(R.id.btn_num9);
        btn0 = (Button) findViewById(R.id.btn_num0);
        btnPoint = (Button) findViewById(R.id.btn_ponto);
        /*Recuperando os botões funcionais*/
        btnCE = (Button) findViewById(R.id.btn_limpa);
        btnIgual = (Button) findViewById(R.id.btn_igual);
        /*Recuperando os botões das operações matemáticas*/
        btnSoma = (Button) findViewById(R.id.btn_mais);
        btnSub = (Button) findViewById(R.id.btn_menos);
        btnDiv = (Button) findViewById(R.id.btn_div);
        btnMult = (Button) findViewById(R.id.btn_mult);
    }

    private void limpaVisor() {
        txtVisor.setText("");
    }

}
