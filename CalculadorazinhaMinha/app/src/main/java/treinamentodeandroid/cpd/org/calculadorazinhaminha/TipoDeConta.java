package treinamentodeandroid.cpd.org.calculadorazinhaminha;

/**
 * Created by gabrielbmoro on 29/04/17.
 */

public enum TipoDeConta {

    SOMA, SUBTRACAO, MULTIPLICACAO, DIVISAO;

}
