# Treinamento Android - CPD


Vamos utilizar este repositório para colocar nele os artefatos gerados durante os nossos encontros. O treinamento é dividido em três partes, cada parte pode levar mais que um encontro, depende de nós :D


  - **Parte A**

    -- Configuração de Ambiente

    -- Entendendo o Ciclo de Vida de uma Activity

    -- Comunicação entre Activities

    -- Utilização do Framework ButterKnife

    -- Utilizando o banco de dados SQLite

  - **Parte B**

    -- Utilização de serviços do Android na App

    -- Primeiro contato com o Framework Dagger

  - **Parte C**

    -- Ideias para organizar melhor nosso código

    -- Utilizando Web service com Retrofit

  - As tarefas a serem realizadas ao decorrer do treinamento estão organizadas neste quadro do Trello: [Quadro de Tarefas](https://trello.com/b/U7zXWI8u/treinamentoandroid-cpd).


### Pré-requisitos de Software

- Java SE Development Kit 8 (JDK 8)
- Android Studio
- Ferramentas da SDK do Android


### Links importantes!

  - [Desenvolvedores Android](https://developer.android.com/)
  - [ButterKnife](http://jakewharton.github.io/butterknife/)
  - [Retrofit](http://square.github.io/retrofit/)
  - [Google Maps para Android](https://developers.google.com/maps/documentation/android-api/?hl=pt-br)
  - [Apostila Android da K19](http://online.k19.com.br/libraries/handouts/k41)



> No que diz respeito ao empenho, ao compromisso, ao esforço, à dedicação, não 
> existe meio termo. Ou você faz uma coisa bem feita ou não faz.
> *Ayrton Senna*